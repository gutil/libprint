/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libprint
 * FILE NAME: itos.c
 *
 * CONTRIBUTORS: Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Joined printing in stderr and files,
 * warning messages and error messages.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libprint Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"

char *LP_answer(int icode, FILE *fp) {
  char *name;

  switch (icode) {
  case YES_LP:
    name = strdup("YES");
    break;
  case NO_LP:
    name = strdup("NO");
    break;
  default: {
    LP_error(fp, "In libprint%4.2f %s line %d: unknown answer\n", VERSION_LP, __FILE__, __LINE__);
    break;
  }
  }

  return name;
}