/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libprint
 * FILE NAME: param_lprint.h
 *
 * CONTRIBUTORS: Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Joined printing in stderr and files,
 * warning messages and error messages.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libprint Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#define STRING_LENGTH_LP 300
#define SEP_LP ","

enum typo { ERR_LP, WARN_LP, NTYPE_LP };
enum answer_fp { YES_LP, NO_LP, NANSWER_LP };

#define VERSION_LP 1.24
#define LLINE_F77_LP 79
#define LLINE_NSAM_LP 75
#define ALLOCSCD_LP 50
