/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libprint
 * FILE NAME: manage_file.c
 *
 * CONTRIBUTORS: Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Joined printing in stderr and files,
 * warning messages and error messages.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libprint Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include "libprint.h"

/**
 *\brief Function to open file in "read" mode.
 *
 *The file must exist!
 *\param name file name to open
 *\param fpout log file name
 *\param lp_type see enum lp_type
 *\return file if succesfulley opened
 */
FILE *LP_openr_file(char *name, FILE *fpout, int lp_type) {
  FILE *fp;

  fp = fopen(name, "r");
  if (fp == NULL) {
    if (lp_type == ERR_LP)
      LP_error(fpout, "Impossible to open file %s\n", name);
    else
      LP_warning(fpout, "Impossible to open file %s\n", name);

  } else
    LP_printf(fpout, "%s successfully opened\n", name);

  return fp;
}