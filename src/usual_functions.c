/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libprint
 * FILE NAME: usual_functions.c
 *
 * CONTRIBUTORS: Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Joined printing in stderr and files,
 * warning messages and error messages.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libprint Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include "libprint.h"

/**
 *\brief print Header to log files
 */
/* Header of the log file */
void LP_log_file_header(FILE *fp, char *progname, double nversion, char *date, char *cmd_file_name) {
  LP_printf(fp, "# %s %4.2f-- Log file\n", progname, nversion);
  LP_printf(fp, "# %s", date);
  LP_printf(fp, "# Command File %s\n\n", cmd_file_name);
}
/**
 *\brief print Header to files
 */
/* Header of the log file */
void LP_file_header(FILE *fp, char *progname, double nversion, char *date) {
  LP_printf(fp, "# %s %4.2f-- Log file\n", progname, nversion);
  LP_printf(fp, "# %s", date);
}
/**
 *\brief to print data in log files
 */
void LP_printf(FILE *fp, char *format, ...) {
  va_list ap;

  va_start(ap, format);
  //(void) vfprintf(stderr,format,ap);
  //(void) vprintf(format,ap);
  (void)vfprintf(fp, format, ap);
  va_end(ap);
  va_start(ap, format);
  (void)vfprintf(stderr, format, ap);
  va_end(ap);
}
/**
 *\brief to print errors in log files
 *
 *The programm will stop after printing the error messages
 */
void LP_error(FILE *fp, char *format, ...) {
  char *to_print;
  va_list ap;

  to_print = (char *)malloc((strlen(format) + 15) * sizeof(char));
  sprintf(to_print, "ERROR: %s\n", format);
  va_start(ap, format); // the last argument of va_start is the last one before the ...
  (void)vprintf(to_print, ap);
  va_end(ap);
  va_start(ap, format); // the last argument of va_start is the last one before the ... in the function arguments
  (void)vfprintf(fp, to_print, ap);
  va_end(ap);

  /*  Simul->clock->end = time(NULL);
  Simul->clock->time_length=difftime(Simul->clock->end,Simul->clock->begin);
  LP_printf("Time of calculation : %f s\n",Simul->clock->time_length);*/
  free(to_print);
  fclose(fp);
  exit(EXIT_FAILURE);
}

/**
 *\brief to print warnings in log files
 *
 *The programm will continu after printing the warning messages
 */
void LP_warning(FILE *fp, char *format, ...) {
  char *to_print;
  va_list ap;

  to_print = (char *)malloc((strlen(format) + 15) * sizeof(char));
  sprintf(to_print, "WARNING: %s\n", format);
  va_start(ap, format); // the last argument of va_start is the last one before the ... in the function arguments
  (void)vprintf(to_print, ap);
  va_end(ap);
  va_start(ap, format); // the last argument of va_start is the last one before the ... in the function arguments
  (void)vfprintf(fp, to_print, ap);
  va_end(ap);
  free(to_print);
}

/**
 *\brief to open files in "write" mode
 *
 *The function will create a new file or errase it if already exists
 */
FILE *LP_openw_file(char *name, FILE *fp) {
  FILE *fpo;

  fpo = fopen(name, "w");
  if (fpo != NULL)
    LP_printf(fp, "File %s opened successfully\n", name);
  else
    LP_error(fp, "Impossible to open file %s\n", name);
  return fpo;
}

/**
 *\brief Puts all the characters of a string in lowercase
 *
 *
 */
/* Puts all the characters of a string in lowercase */
void LP_lowercase(char *name) {
  while (*name) {
    if (isupper(*name))
      *name = tolower(*name);
    name++;
  }
}
