/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libprint
 * FILE NAME: function_lprint.h
 *
 * CONTRIBUTORS: Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Joined printing in stderr and files,
 * warning messages and error messages.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libprint Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

// Most used functions
void LP_printf(FILE *, char *, ...);
void LP_error(FILE *, char *, ...);
void LP_warning(FILE *, char *, ...);

// In manage_file.c
FILE *LP_openr_file(char *, FILE *, int);

// Less used
void LP_log_file_header(FILE *, char *, double, char *, char *);
void LP_file_header(FILE *, char *, double, char *);
FILE *LP_openw_file(char *, FILE *);
int LP_p_file_opened(char *);
void LP_lowercase(char *);
char *LP_valid_name(char *);

// In print_files.c
int LP_jump_line(int, int, FILE *);

// In itos.c
char *LP_answer(int, FILE *);
