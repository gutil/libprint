/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libprint
 * FILE NAME: format_file.c
 *
 * CONTRIBUTORS: Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Joined printing in stderr and files,
 * warning messages and error messages.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libprint Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/** @file format_file.c
 * @brief Functions to format ASCII files */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdarg.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include "libprint.h"

// helps formatting a file which has a limited number of charracters per line. Sends back the number of characters on the line
/** @fn int LP_jump_line(int length,int ljump,FILE *fp)
 * @brief Goes to next line if the number of charracters overrides ljump
 *
 * @arg int length: length of string being written
 * @arg int ljump: maximum length of a line
 * @return the number of characters in the line
 */

int LP_jump_line(int length, int ljump, FILE *fp) {
  int size;

  size = length;
  if (size > ljump) {
    fprintf(fp, "\n");
    size = 0;
  }

  return size;
}